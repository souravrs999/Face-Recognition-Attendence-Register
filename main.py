import os,cv2,time,sys,easygui
import numpy as np
from PIL import Image
from config import *
from pswd_file import *
from datetime import date, datetime
from flask_sqlalchemy import SQLAlchemy
from flask import Flask, render_template, request, url_for, flash, redirect, session, g

app = Flask(__name__)
app.config['SECRET_KEY'] = SECRET_KEY
app.config['SQLALCHEMY_DATABASE_URI'] = DATABASE_URL
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = SQLALCHEMY_WARNINGS
db = SQLAlchemy(app)

class student_id_name_rel(db.Model):
	student_id = db.Column(db.Integer, primary_key=True)
	student_name = db.Column(db.String(30), nullable=False)

class attendence_register(db.Model):
	__table_args__ = {'sqlite_autoincrement': True}

	no = db.Column(db.Integer, primary_key=True)
	student_id = db.Column(db.Integer, unique=False)
	student_name = db.Column(db.String(30), unique=False)
	att_date = db.Column(db.String(20), unique=False)
	period_1_status = db.Column(db.String(10), unique=False)
	period_1_time = db.Column(db.String(10), unique=False)
	period_2_status = db.Column(db.String(10), unique=False)
	period_2_time = db.Column(db.String(10), unique=False)
	period_3_status = db.Column(db.String(10), unique=False)
	period_3_time = db.Column(db.String(10), unique=False)
	period_4_status = db.Column(db.String(10), unique=False)
	period_4_time = db.Column(db.String(10), unique=False)
	period_5_status = db.Column(db.String(10), unique=False)
	period_5_time = db.Column(db.String(10), unique=False)
	period_6_status = db.Column(db.String(10), unique=False)
	period_6_time = db.Column(db.String(10), unique=False)

def get_student_name(student_id):
        name = student_id_name_rel.query.filter(student_id_name_rel.student_id == student_id).first()
        return student_id_name_rel.student_name if hasattr(name, 'student_id') else None

def data_capture_func(student_id, student_name):

	def assure_path_exists(path):
		dir = os.path.dirname(path)
		if not os.path.exists(dir):
		    os.makedirs(dir)

	vid_cam = cv2.VideoCapture(CV2_SOURCE_INDEX)

	face_detector = cv2.CascadeClassifier(CASCADE_FRONTAL)
	count = 0
	assure_path_exists(DATASET_PATH)

	while(True):

	    _, image_frame = vid_cam.read()
	    gray = cv2.cvtColor(image_frame, cv2.COLOR_BGR2GRAY)
	    faces = face_detector.detectMultiScale(gray, 1.3, 5)

	    for (x,y,w,h) in faces:

	        cv2.rectangle(image_frame, (x-20,y-20), (x+w+20,y+h+20), (0,255,0), 2)
	        cv2.putText(image_frame, f'Capturing Faces: {count}/200', (10,20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 1, cv2.LINE_AA)

	        count += 1
	        cv2.imwrite("dataset/" + str(student_name) + '-' + str(student_id) + '-' + str(count) + ".jpg", gray[y:y+h,x:x+w])
	        cv2.imshow('Capturing', image_frame)

	    if cv2.waitKey(100) & 0xFF == ord('q'):
	        break
	    elif count >= 200:
	        break
	vid_cam.release()
	cv2.destroyAllWindows()

def data_train_func():
	recognizer = cv2.face.LBPHFaceRecognizer_create()
	detector= cv2.CascadeClassifier(CASCADE_FRONTAL);

	def getImagesAndLabels(path):

		os.chdir(path)
		faceSamples=[]
		Ids=[]

		for image in os.listdir(os.getcwd()):
		    pilImage=Image.open(image).convert('L')
		    imageNp=np.array(pilImage,'uint8')
		    Id=int(image.split("-")[1])
		    faces=detector.detectMultiScale(imageNp)
		    for (x,y,w,h) in faces:
		        faceSamples.append(imageNp[y:y+h,x:x+w])
		        Ids.append(Id)
		return faceSamples,Ids

	faces,Ids = getImagesAndLabels(DATASET_PATH)
	s = recognizer.train(faces, np.array(Ids))
	os.chdir('../')
	recognizer.write(TRAINER_PATH)	

def live_feed_func():

	def assure_path_exists(path):
	    dir = os.path.dirname(path)
	    if not os.path.exists(dir):
	        os.makedirs(dir)

	recognizer = cv2.face.LBPHFaceRecognizer_create()
	assure_path_exists("trainer/")
	recognizer.read(TRAINER_PATH)
	cascadePath = CASCADE_FRONTAL

	faceCascade = cv2.CascadeClassifier(cascadePath);
	font = cv2.FONT_HERSHEY_SIMPLEX
	cam = cv2.VideoCapture(0)
	frame_height = cam.get(cv2.CAP_PROP_FRAME_HEIGHT)
	
	while True:
	    start = time.time()
	    _, frame = cam.read()
	    ret, im =cam.read()
	    gray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
	    faces = faceCascade.detectMultiScale(gray, 1.2,5)


	    for(x,y,w,h) in faces:
	        cv2.rectangle(im, (x-10,y-10), (x+w+10,y+h+10), (0,255,0), 2, cv2.LINE_AA)
	        Id, confidence = recognizer.predict(gray[y:y+h,x:x+w])

	        try:
	       		student_id = Id
	       		student_name = db.session.query(student_id_name_rel).filter(student_id_name_rel.student_id==Id).first()
	       		student_name = student_name.student_name
	       	except Exception as e:
	       		pass
	       	att_time = datetime.now().strftime('%I:%M')
	        att_date = date.today()

	        # print(student_name.student_name, file=sys.stderr)

	        if confidence >= CONFIDENCE_START_RANGE and confidence <= CONFIDENCE_END_RANGE:

	        	cv2.putText(im, f'Name: {student_name}', (10,20), font, 0.5, (0,255,0), 1, cv2.LINE_AA)
		        cv2.putText(im, f'Reg No: {student_id}', (10,40), font, 0.5, (0,255,0), 1, cv2.LINE_AA)
		        cv2.putText(im, f'Time: {att_time}', (10,60), font, 0.5, (0,255,0), 1, cv2.LINE_AA)
		        cv2.putText(im, f'Date: {att_date}', (10,80), font, 0.5, (0,255,0), 1, cv2.LINE_AA)
		        cv2.putText(im, f'Confidence: {round((confidence*1)/100, 2)}', (10,100), font, 0.5, (0,255,0), 1, cv2.LINE_AA)


		        if bool(attendence_register.query.filter_by(student_id=Id, att_date=att_date).first()) != True:
		        	att = attendence_register(student_id=Id, student_name=student_name, att_date=att_date, period_1_status='', period_1_time='',
			        		period_2_time='', period_2_status='',period_3_time='', period_3_status='',period_4_time='', period_4_status='',
			        		period_5_time='', period_5_status='', period_6_time='', period_6_status='')
			        db.session.add(att)
			        db.session.commit()

		        if float(datetime.now().strftime('%I:%M%S').replace(':','.')) >= PERIOD_1_START_INTERVAL and float(datetime.now().strftime('%I:%M%S').replace(':','.')) <= PERIOD_1_END_INTERVAL:

		        	admin = attendence_register.query.filter_by(student_id=Id, att_date=att_date).update(dict(period_1_status='present', period_1_time=datetime.now().strftime('%I:%M')))
		        	db.session.commit()

		        elif float(datetime.now().strftime('%I:%M%S').replace(':','.')) >= PERIOD_2_START_INTERVAL and float(datetime.now().strftime('%I:%M%S').replace(':','.')) <= PERIOD_2_END_INTERVAL:

		        	admin = attendence_register.query.filter_by(student_id=Id, att_date=att_date).update(dict(period_2_status='present', period_2_time=datetime.now().strftime('%I:%M')))
		        	db.session.commit()

		        elif float(datetime.now().strftime('%I:%M%S').replace(':','.')) >= PERIOD_3_START_INTERVAL and float(datetime.now().strftime('%I:%M%S').replace(':','.')) <= PERIOD_3_END_INTERVAL:

		        	admin = attendence_register.query.filter_by(student_id=Id, att_date=att_date).update(dict(period_3_status='present', period_3_time=datetime.now().strftime('%I:%M')))
		        	db.session.commit()

		        elif float(datetime.now().strftime('%I:%M%S').replace(':','.')) >= PERIOD_4_START_INTERVAL and float(datetime.now().strftime('%I:%M%S').replace(':','.')) <= PERIOD_4_END_INTERVAL:

		        	admin = attendence_register.query.filter_by(student_id=Id, att_date=att_date).update(dict(period_4_status='present', period_4_time=datetime.now().strftime('%I:%M')))
		        	db.session.commit()

		        elif float(datetime.now().strftime('%I:%M%S').replace(':','.')) >= PERIOD_5_START_INTERVAL and float(datetime.now().strftime('%I:%M%S').replace(':','.')) <= PERIOD_5_END_INTERVAL:

		        	admin = attendence_register.query.filter_by(student_id=Id, att_date=att_date).update(dict(period_5_status='present', period_5_time=datetime.now().strftime('%I:%M')))
		        	db.session.commit()

		        elif float(datetime.now().strftime('%I:%M%S').replace(':','.')) >= PERIOD_6_START_INTERVAL and float(datetime.now().strftime('%I:%M%S').replace(':','.')) <= PERIOD_6_END_INTERVAL:

		        	admin = attendence_register.query.filter_by(student_id=Id, att_date=att_date).update(dict(period_6_status='present', period_6_time=datetime.now().strftime('%I:%M')))
		        	db.session.commit()


	    end = time.time()
	    seconds = end - start
	    fps = round(1 / seconds, 2)
	    cv2.putText(im, str(fps), (0, int(frame_height) - 5), font, 0.5, (0, 255, 0), 1, cv2.LINE_AA)

	    cv2.imshow('im',im)

	    if cv2.waitKey(10) & 0xFF == ord('q'):
	    	break

	cam.release()
	cv2.destroyAllWindows()


@app.route('/')
def index():

	return render_template('index.html')

@app.route('/admin_login', methods=['GET','POST'])
def admin_login():
	if request.method == 'GET':
		return render_template('admin_login.html')

	if request.method == 'POST':
		admin_username = request.form['username']
		admin_password = request.form['password']

		if admin_username == ADMIN_PSWD and admin_password == ADMIN_PSWD:
			flash('login successful')
			return redirect(url_for('dashboard'))
		else:
			flash('Wrong Credentials')
			return render_template('admin_login.html')

@app.route('/dashboard')
def dashboard():

	flash('Student has been successfully added to the database..')

	all_data = student_id_name_rel.query.all()
	reg_data = attendence_register.query.all()
	return render_template('dashboard.html', data_stud=all_data, data_reg=reg_data)

@app.route('/data_capture', methods=['GET','POST'])
def data_capture():

	if request.method == 'GET':
		return render_template('data_capture.html')

	if request.method == 'POST':
		student_id = request.form['student_id']
		student_name = request.form['student_name']

		if student_id or student_name == '':
			flash('cannot be empty')
			return redirect('dashboard')

	data_capture_func(student_id, student_name)

	student_details = student_id_name_rel(student_id=student_id, student_name=student_name)
	db.session.add(student_details)
	db.session.commit()

	flash('Student has been successfully added to the database..')
	return redirect(url_for('dashboard'))

@app.route('/data_train')
def data_train():

	data_train_func()
	flash('Training Students List Completed..')
	return redirect(url_for('dashboard'))

@app.route('/live_feed')
def live_feed():

	live_feed_func()
	flash('Results have been saved to database')
	return redirect(url_for('dashboard'))

@app.route('/student_login', methods=['GET','POST'])
def stud_login():

	if request.method == 'GET':
		return render_template('student_login.html')

	if request.method == 'POST':
		session.pop('student_id', None)
		student_id = request.form['student_id']
		student_pswd = request.form['student_pswd']

		ids = student_id_name_rel.query.filter_by(student_id=student_id).first()

		try:
			if int(student_id) == ids.student_id and int(student_pswd) == ids.student_id:
				session['student_id'] = student_id
				flash('Login successful..')
				return redirect(url_for('attendence'))
			else:
				flash('Wrong id or password')
				return render_template('student_login.html')
		except Exception as e:
			flash(f'No student with id {student_id} exist')
			return render_template('student_login.html')

@app.route('/attendence', methods=['GET','POST'])
def attendence():

	if 'student_id' in session:
		student_id = session['student_id']
		print(student_id, file=sys.stderr)
		student_name = student_id_name_rel.query.filter_by(student_id=student_id).first()
		student_name = student_name.student_name
	data_reg = attendence_register.query.filter_by(student_id=student_id).all()
	return render_template('attendence.html', student_name=student_name, att_data=data_reg)

@app.route('/faculty_login', methods=['GET','POST'])
def faculty_login():

	if request.method == 'GET':
		return render_template('faculty_login.html')

	if request.method == 'POST':
		faculty_username = request.form['username']
		faculty_password = request.form['password']
		if faculty_username == FACULTY_PSWD and faculty_password == FACULTY_PSWD:
			flash('login successful')
			return redirect(url_for('faculty_dashboard'))
		else:
			flash('Wrong Credentials')
			return render_template('faculty_login.html')

@app.route('/faculty_dashboard', methods=['GET','POST'])
def faculty_dashboard():
	if request.method == 'GET':
		return render_template('faculty_dashboard.html')

	if request.method == 'POST':

		# print(request.form['period'], file=sys.stderr)
		date = request.form['date']
		period = request.form['period']
		data_reg = attendence_register.query.all()
		all_data = student_id_name_rel.query.all()
		query_date = attendence_register.query.filter_by(att_date=date).all()
		date_tab = attendence_register.query.with_entities(attendence_register.att_date).distinct()

		if period == '1':
			query_prd = attendence_register.query.filter_by(period_1_status='present').all()
			return render_template('faculty_dashboard.html', data_reg=query_prd, date_tab=date_tab,data_stud=all_data)
		if period == '2':
			query_prd = attendence_register.query.filter_by(period_2_status='present').all()
			return render_template('faculty_dashboard.html', data_reg=query_prd, date_tab=date_tab,data_stud=all_data)
		if period == '3':
			query_prd = attendence_register.query.filter_by(period_3_status='present').all()
			return render_template('faculty_dashboard.html', data_reg=query_prd, date_tab=date_tab,data_stud=all_data)
		if period == '4':
			query_prd = attendence_register.query.filter_by(period_4_status='present').all()
			return render_template('faculty_dashboard.html', data_reg=query_prd, date_tab=date_tab,data_stud=all_data)
		if period == '5':
			query_prd = attendence_register.query.filter_by(period_5_status='present').all()
			return render_template('faculty_dashboard.html', data_reg=query_prd, date_tab=date_tab,data_stud=all_data)
		if period == '6':
			query_prd = attendence_register.query.filter_by(period_6_status='present').all()
			return render_template('faculty_dashboard.html', data_reg=query_prd, date_tab=date_tab,data_stud=all_data)
		if date == 'Date':		
			date_tab = attendence_register.query.with_entities(attendence_register.att_date).distinct()
			return render_template('faculty_dashboard.html', data_reg=data_reg, date_tab=date_tab,data_stud=all_data)
		if date != None:
			return render_template('faculty_dashboard.html', data_reg=query_date, date_tab=date_tab,data_stud=all_data)


if __name__ == '__main__':
	db.create_all()
	app.run(debug=DEBUG_STATUS)

